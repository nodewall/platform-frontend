/*
	Development Webpack configuration
*/

const path = require('path')
const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
	mode: 'development',
	entry: {
		entry: path.join(__dirname, '../src/app.js')
	},
	output: {
		filename: '[chunkhash].js',
		path: path.resolve(__dirname, '../dist'),
		publicPath: '/'
	},
	module: {
		rules: [
			{ test: /^\/assets\//, loader: 'ignore-loader' }, /* Ignore lazy-loaded assets */
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env', 'react']
					}
				}
			},
			{
				test: /\.[s]?css$/,
				use: [{
					loader: MiniCssExtractPlugin.loader
				}, {
					loader: 'css-loader'
				}, {
					loader: 'postcss-loader',
					options: {
						config: {path: path.resolve(__dirname)}
					}
				}, {
					loader: 'sass-loader',
					options: {
						includePaths: [path.resolve(__dirname, '../node_modules/')]
					}
				}]
			}
		]
	},
	devServer: {
		contentBase: path.resolve(__dirname, '../dist'),		
		hot: false,
		compress: true,
		port: 8080,
		open: true,
		historyApiFallback: {
			index: '/'
		}
	},
	plugins: [
		new webpack.ProvidePlugin({
			React: 'react',
			Component: ['react', 'Component'],
			PureComponent: ['react', 'PureComponent'],
			ReactDOM: 'react-dom',
			autobind: ['class-autobind', 'default'],
			Context: ['Mod/Context', 'default']
		}),
		new MiniCssExtractPlugin({
			filename: "bundle.css",
			chunkFilename: "[id].css"
		}),
		new CopyWebpackPlugin(
			[
				{
					from: path.resolve(__dirname, '../src/assets'),
					to: path.resolve(__dirname, '../dist/assets')
				}
			]
		),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, '../src/index.html'),
			minify: {
				removeComments: true,
				collapseWhitespace: true
			}
		})
	],
	devtool: 'source-map',
	resolve: {
		alias: {
			Mod: path.resolve(__dirname, '../src/modules'),
			Comp: path.resolve(__dirname, '../src/components'),
			Cont: path.resolve(__dirname, '../src/containers'),
			Ctx: path.resolve(__dirname, '../src/ctx')
		}
	}
}
