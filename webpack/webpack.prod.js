/*
	Production Webpack configuration
*/

/* Require dependencies */
const webpack = require('webpack')
const path = require('path')
const merge = require('webpack-merge')

/* Production plugins */
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const dev = require('./webpack.dev.js')

module.exports =  merge(dev, {
	mode: 'production',
	plugins: [
		new webpack.EnvironmentPlugin({
			NODE_ENV: 'production',
			BUGSNAG_KEY: process.env.BUGSNAG_KEY || false,
			TRACK_BUGS: ['deploy/admin', 'deploy/office'].includes(process.env.BITBUCKET_BRANCH)
		}),
		new CleanWebpackPlugin(path.resolve(__dirname, '../dist/*'), { allowExternal: true })
	],
	optimization: {
		minimizer: [
			new UglifyJSPlugin({
				cache: true,
				parallel: true,
				sourceMap: true,
				uglifyOptions: {
					output: {
						comments: false,
						beautify: false
					}
				}
			}),
			new OptimizeCSSAssetsPlugin()
		]
	}
})
