export default class AppRoot extends Component {
    constructor(props) {
        super(props)
        autobind(this)
    }

    render() {
        return (
            <div>Welcome to the app!</div>
        )
    }
}