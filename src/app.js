/* Bugsnag client configuration */
import ErrorBoundary from 'Mod/Bugsnag'

/* Import globals */
import './globals'

/* Import styles */
import './styles/main.scss'

import App from 'Cont/App'

ReactDOM.render(
    <ErrorBoundary>
        <App/>
    </ErrorBoundary>,
    document.getElementById('root')
)