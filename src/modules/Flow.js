/*  const flow = new Flow()
    
    flow.step(function() {
      // Do anything sync/async
      // ...
      flow.next()
    })

    flow.step(function() {
      // Next step (do anything...)
      // Call next step (if no next step defined, Flow will try last step (see below), otherwise exits).
      flow.next()
    })
    
    // Optional last step to be called when all other steps are done:
    flow.last(function() { console.log('Done!') })
    
    // Start the flow
    flow.start()

    It is possible to pass the context between steps:
    flow.step(() => {
        someAsyncTask((err, result) => {
            flow.next(result)
        })
    })
    flow.step(taskResult => {
        console.log(testResult)
    })
*/

const globalReference = (typeof window == 'undefined') ? global : window

globalReference.Flow = class Flow {
    
    constructor() {
        this.steps = []
        this.lastStep = false

        // Bind instance context to the methods
        this.step = this.step.bind(this)
        this.next = this.next.bind(this)
        this.last = this.last.bind(this)

        this.start = this.next // alias
    }

    step(fn) {
        if (typeof fn !== 'function') {
            throw new Error('[Flow] step() function can only take function as an argument.')
        }
        
        this.steps.push(fn)
    
        return this // chaining allowed    
    }

    next(ctx) {
        this.steps.length
            ? this.steps.shift()(ctx)
            : (this.lastStep || Function.prototype)()
    }

    last(fn) {
        if (typeof fn !== 'function') {
            throw new Error('[Flow] last() function can only take function as an argument.')
        }
        
        this.lastStep = fn
    }

}

