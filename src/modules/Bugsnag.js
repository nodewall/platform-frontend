/**
 * Exposes global reportBug(err) and reportException(exc) functions.
 * Its purpose is to report errors to Bugsnag when necessary even if those
 * errors/exceptions are handled.
 * 
 * Returns ErrorBoundary React HOC.
 */

import bugsnag from 'bugsnag-js'
import createPlugin from 'bugsnag-react'

let ErrorBoundary

if (process.env.TRACK_BUGS && process.env.BUGSNAG_KEY) {
    console.log(`Bugnsnag is active with key ${process.env.BUGSNAG_KEY}`)
    const bugsnagClient = bugsnag({ apiKey: process.env.BUGSNAG_KEY })
    ErrorBoundary = bugsnagClient.use(createPlugin(React))
    window.reportBug = function(...params) {
        bugsnagClient.notify(...params)
    }
    window.reportException = function(...params) {
        bugsnagClient.notifyException(...params)
    }
}
else {
    ErrorBoundary = React.Fragment
    window.reportBug = function(err) {
        console.log('The following error would be reported to Bugsnag if script was running in production:')
        console.log(err)
    }
    window.reportException = function(exc) {
        console.log('The following exception would be reported to Bugsnag if script was running in production:')
        console.log(exc)
    }
}

export default ErrorBoundary