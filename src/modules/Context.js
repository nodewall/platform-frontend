/*
	Context module for React
*/


export default class Context {
	constructor(ctx = {}, actions = {}) {
		this.ctx = ctx
		this.actions = actions
		this.mountedInstances = []
		
		let Context = this
		
		function Connector(Class, propMap) {
			
			class ContextHOC extends React.Component {
				constructor(props) {
					super(props)
			
					this.state = {
						ContextProps: this.getContextProps()
					}
					
					this.ctxUpdate = this.ctxUpdate.bind(this)
				}
				
				render() {
					let ClassProps = {...this.props, ...this.state.ContextProps}
					delete ClassProps.forwardedRef
			
					return (
						<Class {...ClassProps} ref={this.props.forwardedRef}/>
					)
				}
				
				
				getContextProps() {
					let ContextProps = {}
					
					if (typeof propMap === 'function')
						ContextProps = propMap(Context.ctx)
					else if (typeof propMap === 'string')
						ContextProps[propMap] = Context.ctx
					
					return ContextProps
				}
				
				componentDidMount() {
					Context.mountedInstances.push(this)
				}
				
				componentWillUnmount() {
					this.unmounted = true
					Context.mountedInstances = Context.mountedInstances.filter(x => x != this)
				}
				
				ctxUpdate() {
					if (this.unmounted) return
					
					this.setState({
						ContextProps: this.getContextProps()
					})
				}
			}

			let r = React.forwardRef((props, ref) => {
				return <ContextHOC {...props} forwardedRef={ref}/>
			})

			r.function = function(props) {
				return <ContextHOC {...props}/>
			}

			return r
		}
		
		Connector.dispatch = function(action, ...params) {
			if (!Context.actions[action])
				throw new Error(`Context: Undefined action: ${action}`)
			
			Context.ctx = Context.actions[action](Context.ctx, ...params)
			
			Context.updateAll()
		}

		Connector.setState = function(param) {
			let changeObject

			if (typeof param === 'function') {
				changeObject = param(Context.ctx)
			} else {
				changeObject = param
			}

			Context.ctx = {
				...Context.ctx,
				...changeObject
			}

			Context.updateAll()
		}

		Context.updateAll = function() {
			Context.mountedInstances.forEach(instance => {
				instance.ctxUpdate()
			})
		}
		
		return Connector
	}
}
