const boxes = []
const handlers = []

export default {
    addBox(box) {
        // Add box
        boxes.push(box)

        // Attach all handlers to it
        for (let i = 0; i < handlers.length; i++) {
            box.addEventListener('scroll', handlers[i])
        }
    },

    removeBox(box) {
        // Remove box
        const elem = boxes.find(x => x === box)
        if (elem !== -1) boxes.splice(elem, 1)

        // Remove all handlers from it
        for (let i = 0; i < handlers.length; i++) {
            box.removeEventListener('scroll', handlers[i])
        }        
    },

    add(fn) {
        // Add handler
        handlers.push(fn)

        // Attach it to all boxes
        for (let i = 0; i < boxes.length; i++) {
            boxes[i].addEventListener('scroll', fn)
        }
    },
    
    remove(fn) {
        // Remove handler
        const elem = handlers.find(x => x === fn)
        if (elem !== -1) handlers.splice(elem, 1)

        // Remove it from all boxes
        for (let i = 0; i < boxes.length; i++) {
            boxes[i].removeEventListener('scroll', fn)
        }
    }
}