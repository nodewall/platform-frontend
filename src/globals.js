/* Attach these modules to global namespace */

import {BrowserRouter, Route, Link, NavLink, Switch, Redirect} from 'react-router-dom'

window.Router = BrowserRouter
window.Route = Route
window.Link = Link
window.NavLink = NavLink
window.Switch = Switch
window.Redirect = Redirect


/* Global scroll event handling system */
import globalScroll from 'Mod/globalScroll'
window.globalScroll = globalScroll
